import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { TarjetaCredito } from 'src/app/models/TarjetaCredito';
import { TarjetaService } from 'src/app/services/tarjeta.service';

@Component({
  selector: 'app-crear-tarjeta',
  templateUrl: './crear-tarjeta.component.html',
  styleUrls: ['./crear-tarjeta.component.css']
})
export class CrearTarjetaComponent implements OnInit {
  form: UntypedFormGroup;
  loading = false;
  titulo = 'Agregar Tarjeta';
  id: string | undefined;
  boton = 'Aceptar'

  constructor(private fb: UntypedFormBuilder,
              private _tarjetaService: TarjetaService,
              private toastr: ToastrService) {
    this.form = this.fb.group({
      titular: ['', Validators.required],
      numeroTarjeta: ['', [Validators.required, Validators.minLength(12), Validators.maxLength(12)]],
      fechaExpiracion: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(5)]],
      cvv: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]],
    })
   }

  ngOnInit(): void {
    this._tarjetaService.getTarjetaEdit().subscribe(data => {
      this.id = data.id;
      this.titulo = 'Editar Tarjeta';
      this.boton = 'Guardar'
      this.form.patchValue({
        titular: data.titular,
        numeroTarjeta: data.numeroTarjeta,
        fechaExpiracion: data.fechaExpiracion,
        cvv: data.cvv,
      })
    })
  }

  guardarTarjeta() {

    if(this.id === undefined) {
      this.agregarTarjeta();
      this.form.reset();

    } else {
      this.editarTarjeta(this.id);
      this.form.reset();
    }
    
  }

  editarTarjeta(id: string) {
    const TARJETA: any = {
      titular: this.form.value.titular,
      numeroTarjeta: this.form.value.numeroTarjeta,
      fechaExpiracion: this.form.value.fechaExpiracion,
      cvv: this.form.value.cvv,
      fechaActualizacion: new Date(),
    }
    this.loading = true;
    this._tarjetaService.editarTarjeta(id, TARJETA).then(() =>{
      this.loading = false;
      this.titulo = 'Agregar Tarjeta';
      this.boton = 'Aceptar';
      this.form.reset();
      this.id = undefined;
      this.toastr.info('La Tarjeta fue actualizada con exito!', 'Registro Actualizado');
    }, error => {
      console.log(error);
    })
  }

  agregarTarjeta() {
    const TARJETA: TarjetaCredito = {
      titular: this.form.value.titular,
      numeroTarjeta: this.form.value.numeroTarjeta,
      fechaExpiracion: this.form.value.fechaExpiracion,
      cvv: this.form.value.cvv,
      fechaCreacion: new Date(),
      fechaActualizacion: new Date(),
    }
     this._tarjetaService.guardarTarjeta(TARJETA).then(() => {
       console.log('tarjeta registrado');
       this.toastr.success('La tarjete fue registrada con exito!', 'Tarjeta registrada')
       this.form.reset();
     }, error => {
     this.toastr.error('Opps.. ocurrio un error', 'Error');
       console.log(error);
     })
  }
}
